#!/bin/bash

if [[ $# -ne 1 ]]
then
  docker run -v `pwd`:/data idiff:v0.01 
else
  docker run -v `pwd`/`dirname $1`:/data idiff:v0.01 `basename $1`
fi

