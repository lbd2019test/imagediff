# What is idiff?

idiff is a program that compares how different two images are.  It takes a csv file as an argument, that is of the following format.

| image1 | image2 |
|--------|--------|
| aa.png | ab.png |
| ac.png | ad.png |

Upon successful completion it outputs a csv file named outfile.csv in its working directory.  Outfile is of the following format.

| image1 | image2 | similar | elapsed |
|--------|--------|---------|---------|
| aa.png | ab.png |   0.3   | 0.324   |
| ac.png | ad.png |   0.5   | 2.87    |
| aa.png | ad.png |    0    | 0.01    |

If an error is encountered, an error is printed to the terminal window and execution of this program halts immediately.  No output file is generated if an error is encountered.

# Configuration
This application runs in docker, so you will need to install docker for you OS.
[Mac](https://docs.docker.com/docker-for-mac/install/)
[Windows](https://docs.docker.com/docker-for-windows/install/)

Git will be needed for initial build

# Initial Build
git clone https://gitlab.com/lbd2019test/imagediff.git

cd imagediff

docker build -t idiff:v0.01 .

# Usage
Under OSX or Linux, you can use the wrapper script idiff.sh 

or

From the terminal, cd onto the directory containing the input csv file and the image files to be processed and run the following.
docker run -v \`pwd\`:/data idiff:v0.01 input.csv

# Tests
There are some sample images and .csv file which this code has been tested againced.  These test conditions are run upon new code submission.

# Known Issues
Images compared must be the same resolution.
No testing has been done for the following conditions:
- read errors due to lack of permissions
- corrupt image files (eg, bad headers)
- corrupt or improperly formatted csv input file
- lack of permissions to write back file
- lack of available space to write back file

# Development Approach Taken
Initially, I started by working on the image comparison algorithm.  Then insured it produced output within a know range.  After that I had issue installing these libraries on my Mac, so in installed them in a docker container and continued developing with it.  I knew the app needed to travel from OSX to windows, so I thought docker was a good choice.  I started to build a simple dockerfile do install the libraries used based on my initial work within the container.  I got test images together and got the algorithm code working outputting to the terminal off the start.

I then build out from there adding the csv input/output, and simple file checks putting the dockerfile into a closer to final state.  I then came to the realization that docker needs a mount point to access the data, and though I could pass the csv filename into the docker container, I could not read it as the path was wrong.  I attempted to fix that with os.chroot in python, but that had issues with modules used later in the program.  I  worked the path issue but adding a variable within the program and made mapped all data to be read into a /data directory within the container.

I like docker for this project as it simplifies installation of libraries for the user, however it also complicates execution for the user.  I put together a quick wrapper script.  I do not have a windows box, so I did not work on it, however a powershell wrapper or a .bat file could easily wrap around the docker complexity same as in osx/linux.

I will also mention the email instructions state the paths will be absolute, thought the paths in the examples are relative.  I took this to be relative paths will be used.  Under current instructions, the program will only work if no path is provided.  It could be modified if the entire root(/) of the filesystem was exposed to docker.


