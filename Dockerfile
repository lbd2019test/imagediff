FROM jjanzic/docker-python3-opencv:opencv-4.0.1

COPY app /app/
WORKDIR /app

RUN pip install scikit-image==0.15.0
RUN pip install pandas==0.25.0

RUN chmod +x /app/idiff.py

ENTRYPOINT ["/app/idiff.py"]
CMD []

