#!/usr/bin/env python

from skimage import measure
import cv2, sys, pandas, time, os

version = "0.01"

def validateFile(filename):
  #filename passed in should not have the rootdir variable already appended
  if os.path.isfile(rootdir + filename) and os.access(rootdir + filename, os.R_OK):
    return True
  else:
    print("File " + filename + " doesn't exist or is not readable")
    return False

def usage(progname):
  print(progname + " Version " + version)
  print("This program take one agrument, which is a csv filename")
  print("It also requires that the image file for comparison is in the same directories as the csv file passed in.")

if __name__ == "__main__":
  #if program is not running inside docker, make rootdir = '' for paths to work 
  rootdir = '/data/'

  if len(sys.argv) != 2:
    usage(os.path.basename(sys.argv[0]))
    sys.exit(1)

  if validateFile(sys.argv[1]):
    data = pandas.read_csv("/data/" + sys.argv[1])
  else:
    sys.exit(1)

  data['similar'] = -1.1
  data['elapsed'] = 0.0

  for i, x in data.iterrows():
    if validateFile(x.image1):
      i1 = cv2.imread(rootdir + x.image1)
    else:
      sys.exit(1)
    if validateFile(x.image2):
      i2 = cv2.imread(rootdir + x.image2)
    else:
      os.exit(1)
    t = time.process_time()
    # sim returns values between -1 and 1 where 1 is identical
    sim = measure.compare_ssim(i1, i2, multichannel=True)
    # map sim output to be between 0 and +2
    sim = abs(sim - 1)
    elap = time.process_time() - t

    data.loc[i,'similar'] = sim
    data.loc[i,'elapsed'] = elap

  data.to_csv(index=False, float_format='%.2f', path_or_buf=rootdir + "outfile.csv")

